# @swadhi: use control + alt + l to format the code clean!

# 1. Create a dictionary variable "mustang", with following values and print it
# "brand"="Ford",
# "model"="Mustang",
# "year"=1964
mustang = {'brand': "Ford", 'model': "Mustang", "year": 1964}
print(mustang)

# Using the above dictionary, print the below line
# "Mustang" is the model that "Ford" had released in the year - 1964
print(mustang['model'], "is the model that", mustang['brand'], "had released in the year -", mustang["year"])

# 2. Create a dictionary student_ages with below values and print the size of the dictionary
# 'Tim'= 18,'Charlie'=12,'Tiffany'=22,'Robert'=25
student_ages = {'Tim': 18, 'Charlie': 12, 'Tiffany': 22, 'Robert': 25}
print(student_ages)
print(len(student_ages))

# 3. Add two more students, "Sarah"=9 and "John"=21 to the above dictionary
new_students = {'Sarah': 9, 'John': 21}
student_ages.update(new_students)
print(student_ages)

# 4. From the above dictionary, modify the student 'Robert's age using:
#        i. Normal approach
#        ii. Update method
student_ages['Robert'] = 20
print(student_ages)
student_ages.update({'Robert': 30})
print(student_ages)

# 5. To the student_ages dictionary, add any new key which is of type list and note the error
# student_ages={'['1',2,3]':123}
# print(student_ages)
# Output
#  student_ages={'['1',2,3]':123}
# SyntaxError: invalid syntax           @swadhi: TypeError

# 6. Create an empty dictionary ip_address and add the below values to it one by one.
# 'lab'= '192.1.1.1'
# 'ecs'= '192.1.1.2'
# 'bpo'= '192.1.1.3'
# 'rmg'= '192.1.1.4'
ip_address = {}
print(ip_address)
ip_address['lab'] = '192.1.1.1'
print(ip_address)
ip_address['ecs'] = '192.1.1.2'
print(ip_address)
ip_address['bpo'] = '192.1.1.3'
print(ip_address)
ip_address['rmg'] = '192.1.1.4'
print(ip_address)

# 7. To the above dictionary, add three more ip address at one shot.
# 'ise': '192.1.1.5'
# 'wsa': '192.1.1.6'
# 'wsg': '192.1.1.7'
new_dict = {'ise': '192.1.1.5', 'wsa': '192.1.1.6', 'wsg': '192.1.1.7'}
ip_address.update(new_dict)
print(ip_address)
# 8. Write a program that
#   a. uses a data type to store marks of 5 students with name as key and their mark as value
#   b. loop through the dictionary and print the marks alone
student_marks = {'chiru': 20, 'niki': 30, 'govind': 40, 'brinda': 50, 'swadi': 60}
print(student_marks)
for marks in student_marks.values():  # @swadhi: good that you have used 'marks' in variable naming!
    print(marks)

# 9. Write a program that
#   a. Uses a data type to store firstname, lastname and monthy income as key and their corresponding value as values
#   b. Add another key named 'countries_visited' and set empty list as value
#   c. Add three countries to 'countries_visited'
details_dict = {'firstname': "chiru", 'lastname': "duvva", 'income': 30000}
print(details_dict)
details_dict['countries_visited'] = []
print(details_dict)
details_dict['countries_visited'].extend(['india', 'usa', 'uk'])
print(details_dict)

# 10. Write a program that
#   a. Uses a data type to store marks of three students like below and store it in a variable.
#   'student1'=<mark1>,<mark2>
#   'student2'=<mark1>,<mark2>
#   'student3'=<mark1>,<mark2>
#
#   b. For each student add another mark (of your choice to their marks list)
student_dict = {'student1': [30, 40], 'student2': [50, 60], 'student3': [70, 80]}
print(student_dict)
for add in student_dict:
    student_dict[add].append(10)
    print(student_dict)
print(student_dict)
# 11. from the dictionary created in previous question
#   a. loop through student1's mark and print the total
#   b. loop through student2's mark and print the total
#   c. loop through student3's mark and print the total
i = 0  # @swadhi: variable naming should be meaningful. For example: total=0
for mark in student_dict.values():  # x, y and j is not a better practise
    i = i + 1
    print("marks=", mark)
    y = 0
    for x in mark:
        y = y + x

    print("student", i, "total marks", y)

# 12. from the above dictionary print the mark of a student who does not exists and note the error thrown
student_dict = {'student1': [30, 40, 10], 'student2': [50, 60, 10], 'student3': [70, 80, 10]}
print(student_dict)
# print(student_dict['chiru'])

# KeyError: 'chiru'

# 13. from the above dictionary print the mark of a student who does not exists:
#   if the student does not exists then print None
#   if the student does not exists then print 'Student Does Not Exists: <student name>'
if 'chiru' not in student_dict.keys():
    print("None")
    print("Student Does Not Exists: chiru")

# 14. from the above dictionary, print the following:
#     print only the keys
#     print only the values
#     print all the key value pairs as a list of tuples
print(student_dict.keys())
print(student_dict.values())
print(student_dict.items())

# 15. Traverse the above dictionary and print all the student records in below format.
#     student1 has secured the marks [mark1, mark2, mark3]
#     student2 has secured the marks [mark1, mark2, mark3]
#     ...
i = 0
for mark in student_dict.values():
    i = i + 1
    print("student", i, "has secured the marks=", mark)
# 16. Write a program that
#   a. Uses a data type to store languages known by five programmers and store it in a variable
#   (Hint: name is key, value is iterable)
#   b. Loop through the data and print programmer names who knows python (if inside a for loop)
new_programmers = {'chiru': ["C", ], 'niki': ["java", ], 'govind': ["php", ], 'brinda': [".net", ],
                   'swadi': ["python", ]}
for lang in new_programmers.values():
    if "python" in lang:
        print("found")

    # 17. Using the above dictionary, print the programmer names who knows more than 2 programming languages
    if (len(lang) > 2):
        print("is their")

# 18. Use the above dictionary to:
#   a. access an in-existent programmer
#   b. if a programmer don't exists, then print 'ProgrammerNotFoundError'
new_programmers = {'chiru': ["C", ], 'niki': ["java", ], 'govind': ["php", ], 'brinda': [".net", ],
                   'swadi': ["python", ]}
sample = new_programmers.keys()
print(sample)
if "hema" not in sample:
    print("ProgrammerNotFoundError")

# 19. In the above dictionary, use a single command to return the languages
# known by any programmer and also to remove him from the data
print(new_programmers.values())

# 20. Again use a single command to return the languages known by any programmer
# and print the following information
# <programmer name> knows the following languages:
# lang1
# lang2
# lang3
programmer = list(new_programmers.keys())
i = 0
for name in new_programmers.values():
    print(programmer[i], "knows the following languages:")
    i = i + 1
    for lang in name:
        print(lang)

# 21. You want to store a collection of repetitive names to be accessed later on.
# What data type you will use?
tuple_sample = ("a", "b", "c", "a", "b", "c")
print(tuple_sample)

# 22. You want to store a collection of unique phone numbers. Which data type will you use?
# Give example
set_sample = {"a", "b", "c", "a", "b",
              "c"}  # @swadhi: phone numbers? writing incorrect example in test will reduce the score
print(set_sample)

# 23. You are given with a set of constant values such as scores of students in a classroom
# this scores might be used in the future for forecasting purposes. Which data type will you use?
# Dictionaries
# 24. You are provided with roll numbers and total scores of students in class 12th.
# In the future, the data might be accessed based on roll numbers. Which dataype you will use?
# Dictionaries
# 25. You need to store the runs scored by kohli in a dataype such that after every match
# you have to enter a value. Which dataype will you consider?
# list
