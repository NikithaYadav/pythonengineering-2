Questions - 1: Data types

1. Is python dynamically typed Language or statically typed?
Ans: Python is a dynamically typed language.

2. What is dynamic typing means? Is it efficient than statically typed language?
Ans: While creating variables compiler only understand the type of variable
     which we entered for that particular variable and will allocate the memory as well.
     dynamic typing is efficient than static typing.
     @swadhi: Dynamic typing is not efficient. It is subject to runtime errors, TypeError, for example.

3. What is the difference between integer and float variable?
Ans: Integer variables does not contain decimals in the values where float variables contains decimals in the value.
    Ex: a=10 (Integer variable)
        b=12.234 (float variable)

4. Which of these are core data types in python?
    (A) Lists
    (B) Dictionary
    (C) Tuples
    (D) Class
Ans: A, B, C.

5. What data type is the object below ?
    L = [1, 23, ‘hello’, 1]
    (A) List
    (B) Dictionary
    (C) Tuple
    (D) Array
Ans: A

6. Which of the following function display's datatype of variables in python?
    (A) datatype(variable)
    (B) type(variable)
    (C) str(variable)
    (D) id(variable)
Ans: B

String Operations

7. Create a string variable 'text' and print it. The
   output should be: Hello World

   Sample:
       text = '...'
       print(...)

Ans1: text= "Hello World"   # @swadhi: Good that you have used all three ways to define a string
     print(text)
     print(type(text))
Ans2: text= 'Hello World'
     print(text)
     print(type(text))
Ans3: text= '''Hello World'''
     print(text)
     print(type(text))
Ans4: text= """Hello World"""
     print(text)
     print(type(text))

8. Create a string variable 'special_string' and print it. The
   output should be:
   Python is an "object-oriented" Programming language

Ans: special_string='''Python is an "object-oriented" Programming language'''
    print(special_string)
    print(type(special_string))

9. Create a string variable 'multiline_string' and print it. The
   output should be a multiline string with one line spacing.

   Hint: You may use triple quoted string instead of \n new line character

   line 1: Unlike other high level languages
   line 2: <blank line>
   line 3: Python is interpreted

Ans: multiline_string='''Unlike other high level languages
<blank line>
Python is interpreted'''
print(multiline_string)
print(type(multiline_string))

10. Create a string variable "news" with value below:
    Air travel to become bit costlier as govt announces hike in aviation security fee from July 1

    Question: Using string slicing print the below values:
              Air travel
              July 1

    Sample:
      print(news[:n])    empty index at left of : denotes start
      print(news[-n:])   empty index at right of : denotes end
Ans: news="Air travel to become bit costlier as govt announces hike in aviation security fee from July 1"
print(news[:10])
print(news[-6:])

11. Given two string below:

    job_title = "job requirement"
    certs = "certifications in administration is desired"
    added_certs = "additional certifications as below is a plus but not required"
    skills_1 = "azure, windows server"
    skills_2 = "aws, dotnet"

    Use string methods to
      1. capitalize start of each word in job_title
      2. capitalize the first letter of the string certs
      3. capitalize the string completely added_certs
      4. Make the start of each word in skills_1 capitalized
      5. Make the all the words capitalized skills_2
Ans: job_title = "job requirement"
certs = "certifications in administration is desired"
added_certs = "additional certifications as below is a plus but not required"
skills_1 = "azure, windows server"
skills_2 = "aws, dotnet"
print(job_title.title())
print(certs.capitalize())
print(added_certs.upper())
print(skills_1.title())
print(skills_2.upper())

Questions - 2: Datatype definitions

"""I have solved the first question. Please follow the pattern to answer the questions """

 1. Define a string variable "message" with value "Today is a rainy day" and print it.
    Now replace the word "rainy" with "windy" and print it.
Ans: message = "Today is a rainy day"
     print(message)
     message = message.replace('rainy', 'windy')
     print(message)

 2. Define a string variable "name" with your name in all lower case as value and print it.
    Now, use a string method to convert it into all upper case and print it
Ans: name="chiranjeevi"
     print(name)
     new_name= name.upper()
     print(new_name)

 3. Define a string variable "name" with your fullname (firstname<SPACE>lastname) in all lower case as value
    and print it. Now, use a string method to convert each of the first word into capital case and print it.
    Hint: str.title() function
Ans: name="chiranjeevi duvva"
     print(name)
     new_name= name.title()
     print(new_name)

 4. Given the below list, use a string function to convert it into a single string separated by single space.
    usernames = ['swadchan', 'vivevija', 'thasomas', 'verajend']
    Output:     'swadchan vivevija thasomas verajend'
Ans: usernames = ['swadchan', 'vivevija', 'thasomas', 'verajend']
    new_usernames= ' '.join(usernames)
    print(new_usernames)

 5. Given the below list, use a string function to convert it into a single string separated by comma.
    passwords = ['guessme', 'p@$$W0rd', 'hackme']
    Output:     'guessmep@$$W0rdhackme'
Ans: passwords = ['guessme', 'p@$$W0rd', 'hackme']
    new_passwords=''.join(passwords)
    print(new_passwords)

 6. Define a string variable "text" with value "thIs iS a saMPle tEXt" and print it.
    Use a string function to convert it to lowercase and reassign it to 'text'. (text = text.<function>() )
    Now replace the word "a sample" with "an example" and print it.
Ans: text="thIs iS a saMPle tEXt"
    print(text)
    text=text.lower()
    print(text)
    text=text.replace('a sample','an example')
    print(text)

 7. Define a list with any 5 numbers. Add any number to the end of this list. (Hint: list.append() )
Ans: number_list=[1,2,3,4,5]
    number_list.append(6)
    print(number_list)

 8. Define a list with any 5 words (strings). Add any number to the end of this list.
Ans: string_list=['abc','dfg','hij','klm','nop']
      string_list.append(6)
      print(string_list)

 9. Define a list with 3 floating point numbers. Add two more floating point values to the end of this list.
    (Hint: list.extend())
Ans: string_list=[3.34,4.45,5.56]
    print(string_list)
    string_list.extend([6.89,7.67])
    print(string_list)

 10. Given a below list with duplicates.
     browsers = ['chrome', 'firefox', 'safari', 'chrome', 'ie', 'ie']
           a. Print the number of times 'chrome.exe' has appeared in the list.
           b. Sort the list and print it
Ans: browsers = ['chrome', 'firefox', 'safari', 'chrome', 'ie', 'ie']
    print(browsers.count('chrome'))
    browsers.sort()
    print(browsers)

 11. Given a below list.
     processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
           a. Add a new process 'cmd.exe' to the start of the list and print it
           b. Print the number of processes
Ans: processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
    processes.insert(0,'cmd.exe')
    print(processes)
    print(f'size: {len(processes)}')

 11. Given a below list.
     processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
           a. Add a new process 'cmd.exe' to the start of the list (at index 0) and print it  (hint: list.insert)
           b. Add 2 more new processes 'eclipse.exe' and 'norton.exe' to the end and print it (hint: list.extend)
           b. Print the number of processes in the list

Ans: processes = ['chrome.exe', 'mcafee.dll', 'notepad.exe']
    processes.insert(0,'cmd.exe')
    print(processes)
    processes.extend(['eclipse.exe','norton.exe'])
    print(processes)
    print(f'size: {len(processes)}')

 12. Create a tuple with 3 values: 'anand', 30, 'cts'
Ans: new_tuple=('anand', 30, 'cts')

 13. Create a tuple with 1 value: 'anand'
Ans: new_tuple=('anand',)
    print(new_tuple)

 14. Create an empty tuple with 0 values hint: ()
Ans: new_tuple=()
    print(new_tuple)

 15. Create a tuple variable "colors" with below values and print number of times red has occurred.
     (hint: len() will not work)
       red
       green
       green
       orange
       yellow
       yellow
       white
       red
Ans: colors=('red','green','green','orange','yellow','yellow','white','red')
    print(colors.count('red'))

 16. Try to modify the first element of this tuple using below command and see what error occurs.
     colors[0] = 'pink'
Ans: colors[0] = 'pink'
    TypeError: 'tuple' object does not support item assignment

 17. Try to add "colors" by itself as below and print the value.
     (hint: this will create a new tuple. since tuple is immutable)
     colors = colors + colors
Ans: colors=('red','green','green','orange','yellow','yellow','white','red')
    colors=colors+colors
    print(colors)

 18. From the given set of political parties, create a set with variable name "parties" and print it.
     congress
     congress
     bjp dmk
     admk
     dmk
     congress
     admk
     congress
     dmk
Ans: parties={'congress','congress','bjp dmk','admk','dmk','congress','admk','congress','dmk'}
    print(parties)

 19. Given the below list of designations of team members. Find all the unique designations from this list.
     designations = ['developer', 'senior developer', 'developer', 'manager', 'senior developer', 'tester']
Ans: designations = ['developer', 'senior developer', 'developer', 'manager', 'senior developer', 'tester']
    print(set(designations))
 20. Does set allow indexing?
Ans: No

 21. Does list or tuple allow indexing?
Ans: Yes

 22. Given the below "input_set" do the following:
     input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
         a. add a new language "dotnet" to the above set
         b. add the below list of languages to the set
             ['php', 'oracle', 'php', 'swift']
Ans: input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
    input_set.add('dotnet')
    print(input_set)
    input_set.update({'php', 'oracle', 'php', 'swift'})
    print(input_set)

 23. Given the below "input_set" do the following:
     input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
         a. remove the language "dotnet". If not found an error should be thrown
         b. remove the language "dotnet". If not found an error should NOT be thrown
Ans: input_set = {'python', 'java', 'perl', 'java', 'python', 'java'}
    print(input_set.remove('dotnet'))
    print(input_set.discard('dotnet'))

 24. Create a list of numbers from 0 to 9 using range(n) function and print. (do list(range))
Ans: numbers=range(10)
    print(list(numbers))

 25. Create a list of numbers from 10 to 19 using range(n) function
Ans: numbers=range(10,20)
    print(list(numbers))

 26. Create a list of numbers with variable name "list_of_tens" from 10 to 100
     (including 100) in steps of 10 using range(n) function
Ans: list_of_tens=range(10,101,10)
    print(list(list_of_tens))

 27. From the list created in question 26, print the size of the list
Ans: list_of_tens=range(10,101,10)
    print(list(list_of_tens))
    print(len(list_of_tens))

 28. Write a for loop to print the elements in list "list_of_tens" line by lines
Ans: list_of_tens=range(10,101,10)
    for x in list_of_tens:
        print(x)
 29. Enhance this for loop to print only the numbers divisible by 4 and 8.
Ans: list_of_tens=range(10,101,10)
     for x in list_of_tens:
         if x%4==0:
             print(x)
 30. Create a list of odd numbers from 40 to 99.
     Traverse over this loop and print the first number that is divisible by 7
Ans: list_of_odds=range(41,99,2)
     print(list(list_of_odds))
     for x in list_of_odds:
        if x%7==0:
            print(x)
            break

Questions - 3: Lists sets

1. How to convert a list into a tuple? Give example.
Ans: number_list=[1,2,3,4,5]
    new_tuple=tuple(number_list)
    print(new_tuple)

2. How to convert a string into a list? Give example.
Ans: sample_str="chiranjeevi"
    new_list=list(sample_str)
    print(new_list)

3. What is the difference between list's append() method and extend() method. Give example with a simple list.
Ans: append() method will add only one item to the list where as extend() method will add multiple items to the list.
    Ex: new_list=['c', 'h', 'i', 'r', 'a', 'n', 'j', 'e', 'e', 'v', 'i']
        new_list.append('x')
        print(new_list)
        new_list.extend(['y','z'])
        print(new_list)

4. Using "range" function, create a list of numbers with variable name "hundreds" which holds numbers like [100, 200, 300 ... 1000]
(To print this variable you need to type-cast the variable with list() function)
Ans: hundreds=list(range(100,1001,100))
    print(hundreds)

5. Create a list "alpha_list" using square brackets which holds alphabets 'b, c and d' as elements.

    1. After creating the list, Use a list method to insert the letter 'a' at the first
    2. Change the last element into capital [a, b, c, D]. Use last element's index inside square brackets to reassign. hint: l[-1] = str.upper()
    3. Convert this list to a string
    4. Print the size of the list
    5. lowercase the first element
Ans: alpha_list=['b','c','d']
    print(alpha_list)
    alpha_list.insert(0,'a')
    print(alpha_list)
    alpha_list[-1]=str.upper(alpha_list[-1])
    print(alpha_list)
    new_str=''.join(alpha_list)
    print(new_str)
    print("size of list:",len(alpha_list))
    alpha_list[0]=str.lower(alpha_list[0])
    print(alpha_list)

6. Sort the list in-place. The list must now look like: [a, b, c, d]
Ans: alpha_list[-1]=str.lower(alpha_list[-1])
    print(alpha_list)
    alpha_list.sort()
    print(alpha_list)

7. Now add the letters d, e, f, g, h towards the end of the list alpha_list. (list.extend())
Ans: alpha_list.extend(['e','f','g','h'])
    print(alpha_list)

8. Given two lists, find the common numbers in both the lists.

l1 = [1,2,3,4,5]
l2 = [4,5,6,7,8]

    1. convert these lists to two sets, s1 and s2 (type-cast with set() function)
    2. print all the numbers that are common in both the sets. Hint. set1.intersection(set2)
Ans: l1 = [1,2,3,4,5]
    l2 = [4,5,6,7,8]
    s1=set(l1)
    s2=set(l2)
    print('set s1=',s1,'\n','set s2=',s2)
    print(s1.intersection(s2))

9. Create a set with values a, b, c, d and e. Try to add 'a' and 'e' to the list again.

    is the set ordered in the way they are created?
    What happens returned while adding duplicate element to this set?

    How would you avoid error while removing an inexistent element from a set? What was the error?
Ans: is the set ordered in the way they are created?==> No
    What happens returned while adding duplicate element to this set?==> it is not allowing duplicate elements to add to set
    How would you avoid error while removing an inexistent element from a set? What was the error?==> By using discard() method we can avoid error
    i.e.==> sample_set.remove('l')
            KeyError: 'l'